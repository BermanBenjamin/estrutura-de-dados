package br.ucsal.ed;

import java.util.Scanner;

public class String implements IString {

	char[] valores;

	public String() {
		Scanner sc = new Scanner(System.in);
		valores = sc.nextLine().toCharArray();
	}

	public String(char a) {

	}

	public int length() {
		return valores.length;
	}

	public char charAt(int posicao) {
		return valores[posicao];
	}

	public boolean equals(String valor) {
		for (int i = 0; i < valores.length; i++) {
			if (valor.charAt(i) != valores[i]) {
				return false;
			}
		}
		return true;
	}

	public boolean startsWith(String valor) {
		if (valor.charAt(0) == valores[0]) {
			return true;
		} else
			return false;
	}

	public boolean endWith(String valor) {
		for (int i = (valores.length - valor.length()), j = 0; i < valores.length; i++, j++) {
			if (valores[i] != valor.valores[j]) {
				return false;
			}
		}

		return true;
	}

	public int indexOf(char letra) {
		for (int i = 0; i < valores.length; i++) {
			if (valores[i] == letra) {
				return i;
			}
		}
		return -1;
	}

	public int lastIndexOf(char letra) {
		for (int i = valores.length; i > 0; i++) {
			if (valores[i] == letra) {
				return i;
			}
		}
		return -1;
	}

	public String substring(int inicio, int quantidadeDeCaracteres) {
		String palavra = new String('a');
		palavra.valores = new char[quantidadeDeCaracteres];

		for (int i = inicio, j = 0; i < (inicio + quantidadeDeCaracteres); i++, j++) {
			palavra.valores[j] = valores[i];
		}
		return palavra;
	}

	public String replace(char letraASerTrocada, char letraATrocar) {
		for (int i = 0; i < valores.length; i++) {
			if (valores[i] == letraASerTrocada) {
				valores[i] = letraATrocar;
			}
		}
		return this;
	}

	public String concat(String valor) {
		String novo = new String('a');
		novo.valores = new char[(valores.length + valor.valores.length)];
		for (int i = 0; i < valores.length; i++) {
			novo.valores[i] = valores[i];

		}
		for (int i = valor.valores.length; i < novo.valores.length; i++) {
			novo.valores[i] = valor.valores[i];
		}
		return novo;
	}

	public void imprime() {
		for(char l : valores) {
			System.out.print(l);
		}

	}

}
