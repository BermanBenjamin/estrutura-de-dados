package bubbleSort;

import java.util.Scanner;

public class BuscaBinaria {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int tam = sc.nextInt();
		int[] v = new int[tam];
		int cont = 0, busca = 0;
		int num, inicio, fim, meio;

		System.out.println("preencha o vetor: ");
		for (int i = 0; i < v.length; i++) {
			v[i] = sc.nextInt();
		}
		System.out.println("qual numero buscar ?");
		num = sc.nextInt();
		
		// Busca Linear
		System.out.println(BuscaLinear(num, v, cont));
		fim = tam;
		inicio = 0;
		meio = (fim + inicio) / 2;
		// Busca Binaria
		System.out.println(BuscaBinaria(fim, meio, num, v, cont));

	}

	private static int BuscaBinaria(int fim, int meio, int num, int[] v, int cont) {
		int inicio = 0;
		while (fim >= inicio ) {
			if (v[meio] < num) {
				inicio = meio + 1;
				meio = (fim + inicio) / 2 + inicio;
				cont++;
			} else if (v[meio] > num) {
				fim = meio - 1;
				meio = (fim + inicio) / 2 + inicio;
				cont++;
			} else {
				return cont;
			}
		}
		return cont;
		
	}

	private static int BuscaLinear(int num, int[] v, int cont) {
		for (int i = 0; i < v.length; i++) {
			if (v[i] == num) {
				return cont;
			}
			cont++;
		}
		return cont;
	}
	

}
