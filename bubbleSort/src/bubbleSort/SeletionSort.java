package bubbleSort;

import java.util.Scanner;

public class SeletionSort {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int tam = sc.nextInt();
		int[] v = new int[tam];
		int aux;

		for (int i = 0; i < v.length; i++) {
			v[i] = sc.nextInt();
		}
		sc.close();
		System.out.println("Vetor Normal: ");
		for (int i = 0; i < v.length; i++) {
			System.out.print(" " + v[i]);
		}
		for (int i = 0; i < v.length; i++) {
			aux = i;
			for (int j = i; j < v.length; j++) {
				if(v[aux] > v[j]) {
					aux = j;
				}
			}
			int temp = v[i];
			v[i] = v[aux];
			v[aux] = temp;
		}
		System.out.println();
		System.out.println("Vetor Corrigido: ");
		for (int i = 0; i < v.length; i++) {
			System.out.print(" " + v[i]);
		}
	}

}
