package bubbleSort;

import java.util.Scanner;

public class InsertionSort {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int tam = sc.nextInt();
		int[] v = new int[tam];
		int aux;

		for (int i = 0; i < v.length; i++) {
			v[i] = sc.nextInt();
		}
		sc.close();
		System.out.println("Vetor Normal: ");
		for (int i = 0; i < v.length; i++) {
			System.out.print(" " + v[i]);
		}
		for (int i = 0; i < v.length; i++) {
			for (int j = i; j > 0 && v[j] < v[j - 1]; j--) {
				if (v[j] < v[j - 1]) {
					aux = v[j - 1];
					v[j - 1] = v[j];
					v[j] = aux;
				}
			}
		}
		System.out.println();
		System.out.println("Vetor Correto : ");
		for (int i = 0; i < v.length; i++) {
			System.out.print(" " + v[i]);
		}
	}

}
