package bubbleSort;

import java.util.Scanner;

public class BubbleSort {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int tam = sc.nextInt();
		int[] v = new int[tam];
		int aux;

		for (int i = 0; i < v.length; i++) {
			v[i] = sc.nextInt();
		}
		sc.close();
		System.out.println("Vetor Normal: ");
		for (int i = 0; i < v.length; i++) {
			System.out.print(" " + v[i]);
		}
		System.out.println();
		for (int j = 0; j < v.length; j++) {
			for (int i = 0; i < v.length - 1; i++) {
				if (v[i] > v[i + 1]) {
					aux = v[i];
					v[i] = v[i + 1];
					v[i + 1] = aux;
				}
			}
		}
		System.out.println("Vetor Corrigido: ");
		for (int i = 0; i < v.length; i++) {
			System.out.print(" " + v[i]);
		}
	}

}
