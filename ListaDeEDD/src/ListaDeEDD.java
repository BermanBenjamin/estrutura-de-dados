
public class ListaDeEDD implements Lista {
	String minhaLista[] = new String [10];

	String [] novoMaior;

	@Override
	public void Criar(int posicaoDisponivel, String nome) {
		if (posicaoDisponivel > minhaLista.length) {
			EnsureCapacity(posicaoDisponivel * 2);
		} else {
			minhaLista[posicaoDisponivel] = nome;
			posicaoDisponivel++;
		}
	}

	public void EnsureCapacity(int tamanho) {
		if (tamanho < minhaLista.length) {
			System.out.println("seu vetor atual j� � maior");
		} else {
			novoMaior = new String[tamanho];
			for (int i = 0; i < minhaLista.length; i++) {
				novoMaior[i] = minhaLista[i];
			}
		}
	}

	@Override
	public void Atualizar(String palavraExistente,String palavraTroca) {
		for (int i = 0; i < minhaLista.length; i++) {
			if (minhaLista[i].equals(palavraExistente)) {
				minhaLista[i] = palavraTroca;
			}else {
				System.out.println("esta palavra n�o existe no vetor");
			}
		}
	}

	@Override
	public void Excluir(String palavraExcluir) {
		for (int i = 0; i < minhaLista.length; i++) {
			if(minhaLista[i].equals(palavraExcluir)) {
				minhaLista[i] = null;
			}
		}
	}

	@Override
	public void Ler(String palavraEmBusca) {
		for (int i = 0; i < minhaLista.length - 1; i++) {
			if (minhaLista[i].equals(palavraEmBusca)) {
				System.out.println("posic�o da palavra � = " + i);
				return;
			}
		}
	}

}
