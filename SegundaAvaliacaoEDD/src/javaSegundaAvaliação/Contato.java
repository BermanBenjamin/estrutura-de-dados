package javaSegundaAvalia��o;

import java.util.Date;
import java.util.Scanner;

public class Contato {
	public static Scanner sc = new Scanner(System.in);

	String cpf;
	String nome;
	String email;
	String telefone;
	String logradouro;
	String numero;
	String bairro;
	String cidade;
	Date dataDeNascimento;

	public Contato() {
		super();
		String dado = sc.nextLine();
		this.cpf = dado;
		dado = sc.nextLine();
		this.nome = dado;
		dado = sc.nextLine();
		this.email = dado;
		dado = sc.nextLine();
		this.telefone = dado;
		dado = sc.nextLine();
		this.logradouro = dado;
		dado = sc.nextLine();
		this.numero = dado;
		dado = sc.nextLine();
		this.bairro = dado;
		dado = sc.nextLine();
		this.cidade = dado;
		Date dataDeNascimento = null;
		this.dataDeNascimento = dataDeNascimento;
	}
	
	@Override
	public String toString() {
		return "Contato [cpf=" + cpf + ", nome=" + nome + ", email=" + email + ", telefone=" + telefone
				+ ", logradouro=" + logradouro + ", numero=" + numero + ", bairro=" + bairro + ", cidade=" + cidade
				+ ", dataDeNascimento=" + dataDeNascimento + "]";
	}

	public String getCpf() {
		return cpf;
	}


	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public Date getDataDeNascimento() {
		return dataDeNascimento;
	}

	public void setDataDeNascimento(Date dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
	}

}
