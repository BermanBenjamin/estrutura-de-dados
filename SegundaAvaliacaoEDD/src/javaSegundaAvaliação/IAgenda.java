package javaSegundaAvalia��o;

public interface IAgenda {
	
	void Inserir(Contato contato);

	void Remover(String cpf);

	Contato consultarPorCpf(String cpf);

	Contato[] consultarPorBairro(String bairro);
	// retorna todos os contatos do bairro

	Contato[] consultarPorCidade(String cidade);
	// retorna todos os contatos da cidade

	void atualizar(Contato contato);
	// atualiza o contato

	void imprimirContato(Contato contato);
	// imprime o contato (voc� deve buscar o contato com o m�todo consultar por cpf
	// e passar o retorno para o m�todo imprimir)

	void imprimirTodosContatos();
	// imprime todos os contatos

	void ordenaPorCpf(boolean reverse);
	// ordena a lista de contatos por cpf

	void ordenaPorNome(boolean reverse);
	// ordena a lista de contatos por nome

	void ordenaPorDataDeNascimento(boolean reverse);
	// ordena a lista de contatos por data de nascimento
}
